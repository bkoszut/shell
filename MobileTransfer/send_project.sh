#!/bin/sh
TO_HOST="--to-host"
TO_MOBILE="--to-mobile"
USAGE="Usage: $0 project [${TO_HOST}|${TO_MOBILE}]"
HOST_PROJECT_PATH="${WORKSPACE}/gitlab/notes/master"
RESOURCES="res/values*/arrays*"

if [ $# -lt 1 ]
then
  echo $USAGE
  exit 1
fi

if [ $# -ne 1 -a "$2" != "${TO_HOST}" -a "$2" != "${TO_MOBILE}" ]
then
  echo $USAGE
  exit 1
fi

if [ -z "${MOBILE_MOUNT_PATH}" ]
then
  echo "Unset MOBILE_MOUNT_PATH variable"
  exit 1
fi

if [ -z "${MOBILE_NOTE_PATH}" ]
then
  echo "Unset MOBILE_NOTE_PATH variable"
  exit 1
fi

if [ "$2" = "${TO_MOBILE}" ]
then
  target="$2"
else
  target="${TO_HOST}"
fi

if [ ! -d "${HOST_PROJECT_PATH}" ]
then
  echo "Undefined host project path"
  exit 1
fi

project="$1"

if [ ! -d "${HOST_PROJECT_PATH}/${project}" ]
then
  echo "Unknown project at ${HOST_PROJECT_PATH}/${project}"
  exit 1
fi

jmtpfs ${MOBILE_MOUNT_PATH}

if [ -d "${MOBILE_MOUNT_PATH}" ]
then
  echo "Transfering $project $target"
  mobile_mount_dir="$(ls ${MOBILE_MOUNT_PATH})"
  echo "Mobile mount dir: ${mobile_mount_dir}"
  mobile_project_path="${MOBILE_MOUNT_PATH}/${mobile_mount_dir}/${MOBILE_NOTE_PATH}"
  echo "Mobile project path: ${mobile_project_path}"
  if [ -d "${mobile_project_path}" ]
  then
    if [ "${target}" = "${TO_MOBILE}" ]
    then
      cd "${HOST_PROJECT_PATH}"
      cp --parents ${project}/${RESOURCES} "${mobile_project_path}"
      cd - >/dev/null
      sleep 2
    fi
    if [ "${target}" = "${TO_HOST}" ]
    then
      cd "${mobile_project_path}"
      cp --parents ${project}/${RESOURCES} "${HOST_PROJECT_PATH}"
      cd - >/dev/null
      sleep 2
    fi
  else
    echo "Unknown mobile device path at ${mobile_project_path}"
  fi
else
  echo "Mobile device not mounted"
fi

fusermount -u ${MOBILE_MOUNT_PATH}

