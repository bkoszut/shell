#!/bin/sh
#
for CERT in \
  imap.gmail.com:993 \
  imap.mail.yahoo.com:993
do
  echo |\
  openssl s_client -connect ${CERT} 2>/dev/null |\
  sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' |\
  openssl x509 -noout -subject -dates
done
