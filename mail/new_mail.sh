#!/bin/sh

new_mail=$(echo 'x' | mail | grep '^[ |>[N|U]')

if [ ! -z "$new_mail" ]
then
  notify-send 'You have new mail'
fi

