# Mail Client

![](mail.png)

## IMAP mail configuration

    read -p "Mail server (imap.mail.yahoo.com): " host
    read -p "Mail port (993): " port
    read -p "Username: " username
    read -p "Password (app password): " -s password

## IMAP mail search

    sh -c " \
    echo 23 login ${username} ${password}; \
    echo 23 select Inbox; \
    echo 23 search since 1-Jan-2024 from admin; \
    echo 23 logout; \
    sleep 8;" | openssl s_client -connect ${host}:${port}

## IMAP mail fetch

    read -p "Message id: " msgid
    sh -c " \
    echo 23 login ${username} ${password}; \
    echo 23 select Inbox; \
    echo 23 fetch ${msgid} body[header.fields (to date subject)]; \
    echo 23 fetch ${msgid} body[text]<0.2048>; \
    echo 23 logout; \
    sleep 8;" | openssl s_client -connect ${host}:${port}

