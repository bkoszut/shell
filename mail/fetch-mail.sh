#!/bin/bash

HOST_NAME="imap.mail.yahoo.com"
HOST_PORT=993
FOLDER="Inbox"
SESSION="23"
MESSAGES="1"

if [ $# -eq 0 ]
then
  echo "Usage: $0 message-ids-file | msg-id-1 msg-id-2 ..."
  exit 1
else
  if [ -f $1 ]
  then
    msgids=$(cat $1)
  else
    msgids=$@
    for msgid in $msgids
    do
      if ! [[ "$msgid" =~ ^[0-9]+$ ]]
      then
        echo "Invalid message id: $msgid"
        exit 1
      fi
    done
  fi
fi
read -p "Mail server ($HOST_NAME): " hostname
if [ -z $hostname ]; then hostname=$HOST_NAME; fi
read -p "Mail port ($HOST_PORT): " hostport
if [ -z $hostport ]; then hostport=$HOST_PORT; fi
read -p "Username: " username
read -p "Password (app passwd): " -s password
echo
read -p "Folder ($FOLDER): " folder
if [ -z $folder ]; then folder=$FOLDER; fi

fetch_message() {
  message=$1
  folder=$2
  echo "Fetching message $message"
  sh -c " \
  echo \"$SESSION login $username $password\"; \
  echo \"$SESSION select $folder\"; \
  echo \"$SESSION fetch $message body[header.fields (to date subject)]\"; \
  echo \"$SESSION fetch $message body[text]<0.2048>\"; \
  echo \"$SESSION logout\"; \
  sleep 2;" | openssl s_client -connect ${hostname}:${hostport}
  sleep 4
}

for message in $msgids
do
  fetch_message $message $folder
done
