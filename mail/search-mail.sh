#!/bin/bash

HOST_NAME="imap.mail.yahoo.com"
HOST_PORT=993
FOLDER="Inbox"
SESSION="23"
SEARCH_SINCE="1-Jan-2012"

read -p "Mail server ($HOST_NAME): " hostname
if [ -z $hostname ]; then hostname=$HOST_NAME; fi
read -p "Mail port ($HOST_PORT): " hostport
if [ -z $hostport ]; then hostport=$HOST_PORT; fi
read -p "Username: " username
read -p "Password: (app passwd) " -s password
echo
read -p "Folder ($FOLDER): " folder
if [ -z $folder ]; then folder=$FOLDER; fi
read -p "Search messages from: " from
read -p "Search messages since ($SEARCH_SINCE): " since
if [ -z $since ]; then since=$SEARCH_SINCE; fi

echo "Searching messages"
sh -c " \
echo \"$SESSION login $username $password\"; \
echo \"$SESSION select $folder\"; \
echo \"$SESSION search since $since from $from\"; \
echo \"$SESSION logout\"; \
sleep 8;" | openssl s_client -connect ${hostname}:${hostport}
