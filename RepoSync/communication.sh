#! /bin/sh

CONNECT=connect
DISCONNECT=disconnect
HOST1_NAME=PJem
HOST2_NAME=TiMur
MOUNT_POINT=/media
HOME=/home
DATA=data

if [ $# -ne 2 -o "$1" != "${CONNECT}" -a "$1" != "${DISCONNECT}" ]
then
  echo "Usage: $0 ${CONNECT} | ${DISCONNECT} entity"
  exit 1
fi

if [ "$2" != "${HOST1_NAME}" -a "$2" != "${HOST2_NAME}" ]
then
  echo "Uknown entity $2"
  exit 1
fi

if [ "$2" = "$(hostname)" ]
then
  echo "The host is $2"
  exit 1
fi

mount_point="${MOUNT_POINT}/$2"
home="${HOME}/$2/${DATA}"

if [ "$1" = "${CONNECT}" ]
then
  #echo "Connecting to $2"
  mount ${mount_point} > /dev/null 2>&1
  if [ $? -ne 0 -a $? -ne 32 ]
  then
    echo "Unable to connect at ${mount_point} ($?)" 1>&2
    exit 1
  fi
  if [ -d ${mount_point}/${DATA} ]
  then
    mount_point=${mount_point}/${DATA}
  fi
  sudo mount --bind ${mount_point} ${home} > /dev/null 2>&1
  if [ $? != 0 ]
  then
    echo "Unable to connect at ${home} ($?)" 1>&2
    exit 1
  fi
fi

if [ "$1" = "${DISCONNECT}" ]
then
  #echo "Disconnecting from $2"
  sudo umount ${home} > /dev/null 2>&1
  if [ $? != 0 ]
  then
    echo "Unable to disconnect from ${home} ($?)" 1>&2
  fi
  umount ${mount_point} > /dev/null 2>&1
  if [ $? != 0 ]
  then
    echo "Unable to disconnect from ${mount_point} ($?)" 1>&2
  fi
fi
