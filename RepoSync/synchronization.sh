#! /bin/bash

HOST1_NAME=PJem
HOST2_NAME=TiMur
HOME=/home
DATA=data

local_data="/${DATA}"

if [ "$(hostname)" = "${HOST1_NAME}" ]
then
  docked_data="${HOME}/${HOST2_NAME}/${DATA}"
elif [ "$(hostname)" = "${HOST2_NAME}" ]
then
  docked_data="${HOME}/${HOST1_NAME}/${DATA}"
else
  echo "Unknown docking entity"
  exit 0
fi

if [ ! -d "${local_data}" ]
then
  echo "Local data path is unknown"
  exit 0
fi

if [ ! -d "${docked_data}" ]
then
  echo "Docking data path is unknown"
  exit 0
fi

if [ -z "$(ls -A ${local_data})" ]
then
  echo "Local data path is empty"
  exit 0
fi

if [ -z "$(ls -A ${docked_data})" ]
then
  echo "Docking data path is empty"
  exit 0
fi

weigh() {
  weight=$(ls -ld  $1|sed "s/^[^ ]* [^ ]* [^ ]* [^ ]* *//"|sed "s/ .*//")
}

user() {
  uid=$(ls -ldn  $1|sed "s/^[^ ]* [^ ]* *//"|sed "s/ .*//")
}

group() {
  gid=$(ls -ldn  $1|sed "s/^[^ ]* [^ ]* [^ ]* *//"|sed "s/ .*//")
}

age() {
  seconds=$(date -r $1 +%s)
}

copy() {
  src_file=$1
  dst_file=$2
  if [ ! -e "${dst_file}" ]
  then
    if [ -d "${src_file}" ]
    then
      su -c "mkdir ${dst_file}" $(id -nu ${uid})
    else
      su -c "cp ${src_file} ${dst_file}" $(id -nu ${uid})
    fi
  else
    if [ ! -d "${src_file}" -o ! -d "${dst_file}" ]
    then
      if [ ! -z "$(diff -q ${src_file} ${dst_file})" ]
      then
        if [ ${src_file} -nt ${dst_file} ]
        then
          su -c "cp -r ${src_file} ${dst_file}" $(id -nu ${uid})
        fi
      fi
    fi
  fi
}

replicate() {
  file_path=$1
  src_data=$2
  dst_data=$3
  user ${dst_data}/${file_path}
  group ${dst_data}/${file_path}
  echo "$(date +'%Y-%m-%d %H:%M') Replicating ${src_data}/${file_path} to ${dst_data}/${file_path} owned by ${gid}:${uid}"
  for file in $(find ${src_data}/${file_path} | sed "s|^${src_data}/||")
  do
    copy "${src_data}/${file}" "${dst_data}/${file}"
  done
  for file in $(find ${dst_data}/${file_path} | sed "s|^${dst_data}/||")
  do
    if [ ! -e "${src_data}/${file}" ]
    then
      if [ ! -z "${dst_data}" -a ! -O ${dst_data}/${file} ]
      then 
        su -c "rm -r ${dst_data}/${file}" $(id -nu ${uid})
      fi
    fi
  done
  #chown -R ${gid}:${uid} ${dst_data}/${file_path}
}

synchronize() {
  file_path=$1
  esc_file_path=$(echo $file_path|sed 's/\//\\\//g')
  local_path="${local_data}/${file_path}"
  docked_path="${docked_data}/${file_path}"
  local_diff_newer=0
  docked_diff_newer=0
  differences=$(diff -rq ${local_path} ${docked_path})
  if [ ! -z "$differences" ]
  then
    while read -r difference
    do
      only=$(echo "$difference"|grep "Only")
      differ=$(echo "$difference"|grep "differ")
      local_diff=$(echo $difference|grep " ${local_path}")
      docked_diff=$(echo $difference|grep " ${docked_path}")
      if [ ! -z "$only" ]
      then
        ##### check parent dir date to see if the file was created or deleted #####
        if [ ! -z "$local_diff" -a -z "$docked_diff" ]
        then
          local_diff_newer=$((local_diff_newer+1))
        fi
        if [ ! -z "$docked_diff" -a -z "$local_diff" ]
        then
          docked_diff_newer=$((docked_diff_newer+1))
        fi
      fi
      if [ ! -z "$differ" ]
      then
        file=$(echo $difference|sed s'/ differ//'|sed 's/.* //'|sed "s/.*$esc_file_path//")
        if [ -e ${local_path}${file} -a -e ${docked_path}${file} ]
        then
          if [  ${local_path}${file} -nt ${docked_path}${file} ]
          then
            local_diff_newer=$((local_diff_newer+1))
          else
            docked_diff_newer=$((docked_diff_newer+1))
          fi
        fi
      fi
    done <<< "$differences"
    echo "Number of local files newer than docked files" ${local_diff_newer} >&2
    echo "Number of docked files newer than local files" ${docked_diff_newer} >&2
    if [ ${local_diff_newer} -gt 0 -a ${docked_diff_newer} -eq 0 ]
    then
      replicate ${file_path} ${local_data} ${docked_data}
    elif [ ${local_diff_newer} -eq 0 -a ${docked_diff_newer} -gt 0 ]
    then
      replicate ${file_path} ${docked_data} ${local_data}
    elif [ ${local_diff_newer} -gt 0 -a ${docked_diff_newer} -gt 0 ]
    then
      echo "$(date +'%Y-%m-%d %H:%M') Merge required for ${file_path}"
      if [ ! -z "${MAIL_USER}" ]
      then
        msg="Repository synchronization requires merge for ${file_path}."
        echo $msg | mail -s "Repository synchronization" ${MAIL_USER}
      fi
    fi
  fi
}

synchronize "lab/python/course"
synchronize "lab/python/harmony"
#synchronize "lab/shell"
