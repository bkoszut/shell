# Repository Synchronization

Synchronization between local and docked data repositories

[communication.sh](communication.sh)
--------------------------------
Logical connection between two devices

[synchronization.sh](synchroization.sh)
--------------------------------
File synchronization between two devices

