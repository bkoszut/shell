# Audio and Video

![](playback.png)

## List of audio devices

    arecord -l # card 1, device 0

## Audio recorder

    arecord -f cd --device="hw:1,0" audio.wav

## Audio player

    aplay audio.wav

## Video converter

    ffmpeg -i video.avi video.mp4

## Screen video recorder

    ffmpeg -f x11grab -i :0.0 video.mp4

## Movie player

    mplayer -zoom -xy 2 video.mp4

