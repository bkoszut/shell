# Code Assistant

![](opensource.png)

## Python documentation

    pydoc -b -p 8000

## Documentation on a module

    pydoc re

## Python HTTP server

    python -m http.server -port 8000

## Java API documentation

    javadoc -d dest -sourcepath source -subpackages javax.xml

## Java source code decompiler

    jad -o -s java -d dest Mystery.class

