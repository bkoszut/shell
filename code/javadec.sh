#! /bin/sh

JAVA_DIR="src";

if [ $# -ne 1 ]
then
  echo "Usage: $0 path_to_source_directory"
  exit 0
fi

if [ ! -e $1 ]
then
  echo "No such file $1"
  exit 0
fi

if [ -e $JAVA_DIR ]
then
  echo "Warning: $JAVA_DIR already exists in current directory"
  echo "Do you want to proceed? (Y/N)"
  read response
  if [ ! "$response" = "Y" -a ! "$response" = "y" ]
  then
    exit 0
  fi
fi

if [ -f $1 ]
then
  echo "Decompiling file $1"
  dir="$JAVA_DIR/`echo $1|sed 's/\/[^\/]*$//'`"
  if [ ! -d $dir ]
  then
    mkdir -p $dir
  fi
  jad -o -s java -d $dir $1
  exit 0
fi

decompile_files_in_dir() {
  echo "Decompiling files in directory $1"
  for file in `find $1`
  do
    if [ -d $file -a ! "$file" = "$1" ]
    then
      decompile_files_in_dir $file
    fi
    if [ -f $file -a "`echo $file|sed 's/.*\.//'`" = "class" ]
    then
      echo "Decompiling file $file"
      dir="$JAVA_DIR/`echo $file|sed 's/\/[^\/]*$//'`"
      if [ ! -d $dir ]
      then
        mkdir -p $dir
      fi
      jad -o -s java -d $dir $file
    fi
  done
}

if [ -d $1 ]
then
  decompile_files_in_dir $1
fi
