#! /bin/sh

DOCS_DIR='/tmp/docs'

if [ $# -ne 1 ]
then
  echo "Usage: $0 path_to_source_directory"
  exit 0
fi

if [ ! -d $1 ]
then
  echo "No such directory $1"
  exit 0
fi

path=$(echo $1|sed 's/\/$//')
escPath=$(echo $path|sed 's/\//\\\//g')
packages=''
for pkg in `find $path -type d|sed "s/$escPath//"|sed 's/\//./g'|sed 's/^\.//'`
do
  packages="${packages}${pkg} "
done
topPkg=$(echo $packages|sed 's/ .*//')
cd $path
javadoc -d ${DOCS_DIR} -subpackages ${packages} ${topPkg}
