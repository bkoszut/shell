# Text Data

![](markdown.png)

## Source from another file

    bash -c "source var.properties"
    sh -c ". dep.sh"

## File type

    file text.txt

    dos2unix text.txt # newline CRLF to LF
    unix2dos text.txt # newline LF to CRLF

## File format

    docx2txt text.docx # plain text
    xlsx2csv data.xlsx # comma separated values

## Document type

    pandoc -t plain README.md | less
    pandoc -t pdf README.md -o README.pdf
    pandoc -s -t man README.md | man -l -
    pandoc -s -f markdown -t html README.md -o README.html
    pandoc -s --template template.html README.md
    pandoc --print-default-template html

## Reading from standard input

    read -p 'Username: ' username
    read -p 'Password: ' -s password
    stty -echo; read -p 'Password: ' password; stty echo

## Border lines of text

    head -3 text.txt
    tail -f text.txt

## Filtered lines of text

    grep -i Pattern text.txt

## Selected lines of text

    sed -n '21,23p' text.txt # lines 21 through 23

## Sorted lines of text

    cat text.txt | sort --unique --reverse

## Text length or character count

    echo $(($(echo "Internationalization" | wc -c) - 1))

## Character at a position

    echo "Internationalization" | cut -c20

## Regular expression group

    echo "cat;brown;3" | sed 's/^.*;\([^;]*\);.*$/\1/'
