#!/bin/bash

HELP="--help"
INDEXED_ARRAY="--indexed"
ASSOCIATIVE_ARRAY="--associative"
USAGE="Usage: $0"

if [ $# -ne 0 ]
then
  echo $USAGE
  exit 1
fi

indexed_array() {
  echo "Indexed array (one three four)"
  unset array
  array=(one two three)
  array[3]=four
  unset array[1]
  echo " Length: ${#array[*]}"
  echo " Indexes: "; for index in "${!array[*]}"; do echo $index; done
  echo " Values: "; for value in "${array[*]}"; do echo $value; done
}

associative_array() {
  echo "Associative array ([first]=one [thrid]=three)"
  unset array
  declare -A array
  array=([first]=one [second]=two)
  array[third]=three
  unset array[second]
  echo " Length: ${#array[*]}"
  echo " Keys: "; for key in "${!array[*]}"; do echo $key; done
  echo " Values: "; for value in "${array[*]}"; do echo $value; done
}

indexed_array
associative_array
