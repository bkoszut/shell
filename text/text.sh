#!/bin/sh

HELP="--help"
LENGTH="--length"
CHAR_AT="--char-at"
USAGE="Usage: $0 $LENGTH text | $CHAR_AT position text"

if [ $# -lt 1 ]
then
  echo $USAGE
  exit 1
fi

if [ "$1" != "$HELP" -a "$1" != "$LENGTH" -a "$1" != "$CHAR_AT" ]
then
  echo $USAGE
  exit 1
fi

if [ "$1" = "$HELP" ]
then
  echo $USAGE
  exit 0
fi

length() {
  len=`echo $@ | wc -c`
  result=$((len-1))
}

if [ "$1" = "$LENGTH" ]
then
  if [ $# -ne 2 ]
  then
    echo $USAGE
    exit 1
  fi
  length $2; echo $result
fi

charAt() {
  position=$1
  shift
  result=`echo $@ | cut -c$position`
}

if [ "$1" = "$CHAR_AT" ]
then
  if [ $# -ne 3 -o $(($2+1)) -le 1 ]
  then
    echo $USAGE
    exit 1
  fi
  charAt $2 $3; echo $result
fi

