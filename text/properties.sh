#!/bin/sh

USAGE="Usage: . $0 properties-file"

if [ $# -ne 1 ]
then
  echo $USAGE
  exit 1
fi

file=$1

if [ ! -f $file ]
then
  echo "No such file"
  exit 1
fi

while IFS='=' read -r key value
do
  if [ "$(echo $key | cut -c1)" = "#" -o -z "$key" ]
  then
    continue
  fi
  key=$(echo $key | tr '.' '_')
  eval "${key}='${value}'"
  echo "${key}=${value}"
done < "$file"
