#!/bin/sh

HELP="--help"
YESNO="yesno"
PASSWORD="password"
USAGE="Usage: $0 $YESNO | $PASSWORD"

if [ $# -lt 1 ]
then
  echo $USAGE
  exit 1
fi

if [ "$1" != "$HELP" -a "$1" != "$YESNO" -a "$1" != "$PASSWORD" ]
then
  echo $USAGE
  exit 1
fi

if [ "$1" = "$HELP" ]
then
  echo $USAGE
  exit 0
fi

prompt_yesno() {
  while true; do
    read -p "Do you want to proceed? [Y/n] " yn
    if [ -z "$yn" ]; then
      yn="Y"
    fi
    case $yn in
      [Yy] ) echo "Proceeding."; break;;
      [Nn] ) echo "Cancelling."; exit;;
      * ) echo "???";;
    esac
  done
}

hidden() {
  i=0
  word=$1
  hidden_word=""
  while [ $i -lt ${#word} ]
  do
    hidden_word="${hidden_word}*"
    i=$(expr $i + 1)
  done
}

prompt_password() {
  read -p 'Username: ' username
  stty -echo; read -p 'Password: ' password; stty echo; echo ""
  hidden $password
  echo "Welcome ${username} (${hidden_word})."
}

if [ "$1" = "$YESNO" ]
then
  prompt_yesno
fi

if [ "$1" = "$PASSWORD" ]
then
  prompt_password
fi

