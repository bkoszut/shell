# Docker

#### Container Architecture

![](docker.png)

## Greeting C code for an image

    #include <stdio.h>
    int main(void) {
        printf("Hello World");
        return 0;
    }

## Code compilation with static library

    gcc -static -o hello hello.c

## Docker file for an image

    FROM scratch
    ADD hello /
    CMD ["/hello"]

## Building an image with a tag

    docker build -t bkoszut/anchor:v1 .

## List of images

    docker images

## Running a container for an image

    docker run --name greeting bkoszut/anchor:v1

## List of all containers

    docker ps -a

## List of running containers

    docker ps

## Stopping a container

    docker stop greeting

## Removing a container

    docker rm greeting

## Uploading an image to docker hub

    docker login
    docker push bkoszut/anchor
    docker logout

## Removing an image

    docker rmi bkoszut/anchor:v1

## Downloading an image from docker hub

    docker pull bkoszut/anchor

