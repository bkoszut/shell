#!/bin/sh

MARKDOWN_DIR=
PAGES_DIR=pages

if [ $# -ne 1 -o "$(echo $1|sed 's/\.md$//').md" != "$1" ]
then
  echo "Usage $0 filename.md"
  exit 1
fi
if [ ! -f "$1" ]
then
  echo "File $1 does not exist"
  exit 1
fi
file=$1
echo "Converting markdown to html $file"
script_path=$(cd "$(dirname "$0")"; pwd -P)
file_path=$(cd "$(dirname "$1")"; pwd -P)
file_name=$(echo $1 | sed 's/^\.\///' | sed 's/.*\///g')
cd $script_path
script_path=$(echo $script_path | sed 's/\//\\\//g')
file=$(echo ${file_path}"/" | sed "s/${script_path}//" | sed "s/\/${MARKDOWN_DIR}//" | sed 's/^\///')$file_name
file=$(echo $file|sed 's/\.md$//')
if [ ! -z "${MARKDOWN_DIR}" ]
then
  MARKDOWN_DIR="${MARKDOWN_DIR}/"
fi
#markdown ${MARKDOWN_DIR}${file}.md > ../${PAGES_DIR}/${file}.html
pandoc -t html ${MARKDOWN_DIR}${file}.md > ../${PAGES_DIR}/${file}.html
if [ "$(echo $file|sed 's/.*\///')" = "README" ]
then
  index=$(echo $file|sed 's/README/index/')
  mv ../${PAGES_DIR}/${file}.html ../${PAGES_DIR}/${index}.html
  file=${index}
fi
cd ../${PAGES_DIR}
./renderer.sh ${file}.html

