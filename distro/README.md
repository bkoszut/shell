# Linux Distribution

![](ubuntu.png)

## Kernel information

    uname -a

## Operating system release

    cat /etc/os-release

## Release upgrade

    apt update
    apt dist-upgrade
    do-release-upgrade

## Start disk creation

    cp linux.iso /dev/sdc # unmounted disk
    fdisk /dev/sdc # [a] - toggle bootable

## Initial root password

    sudo passwd root

## System services

    systemctl disable udisks2 # automount
    systemctl mask udisks2.service

## Graphical display

    systemctl stop lightdm.service
    lightdm --test-mode --debug
    dpkg --force-configure-all --configure lightdm
    startx

## System journal

    journalctl -b0 # last boot
    systemctl --user status --state=failed

