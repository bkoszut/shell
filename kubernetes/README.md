# Kubernetes

#### Container Orchestrator for Distributed Systems

![](kubernetes.png)

## KinD (Kubernetes in Docker)

KinD runs Kubernetes clusters of nodes using Docker containers.

    docker pull kindest/node

## Creating a cluster of nodes

    kind create cluster --name bridge

## List of clusters

    kind get clusters

## Control plane IP address

    kubectl cluster-info --context kind-bridge

## Cluster configuration

    kubectl config view

## Loading an image to a cluster

    kind load docker-image 3ce241d43d88 --name bridge

## List of running nodes

    docker ps

## List of images on a cluster node

    docker exec -it bridge-control-plane crictl images

## Deploying an application workload

    kubectl create deployment hello --image=greeting

## List of deployments

    kubectl get deployments

## List of pods

    kubectl get pods

## List of events

    kubectl get events

## Pod log

    kubectl logs hello-5c8b45999f-5tkn6

## Deleting a deployment

    kubectl delete deployment hello

## Deleting a cluster

    kind delete cluster --name bridge




