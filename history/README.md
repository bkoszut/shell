# Content Version Control

![](git.png)

## Creating a new repository

    git init shell

## Cloning a repository

    git clone https://gitlab.com/bkoszut/shell.git pages

## Creating a branch

    git branch pages

## Switching to a branch

    git switch pages

## Pushing a new local branch to a remote repository

    git push -u origin pages

## Adding a file to the local repository

    git add README.md

## Marking content modifications as committed to the repository

    git commit -m "Markdown support"

## Pushing modifications to the remote repository

    git push

## Pulling new modifications from the remote repository

    git pull

## Displaying status of the local version of content

    git status

## History of last 4 commits

    git log -4 --oneline

## Commit history with a specific message

    git log --grep data

## Commit history for a given path

    git log --pretty=medium -- text/README.md

## Content changes in the last commit

    git show $(git log -1 --pretty=%h)

## Difference between working tree and index

    git diff text/README.md
